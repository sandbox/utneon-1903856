Description
-----------
This module provides integration between http://drupal.org/project/fboauth and
http://drupal.org/project/services. You will find this most useful if you're developing
an iOS Mobile Application, Android App or HTML5 Mobile App with PhoneGap.

Requirements
------------
Drupal 7.x

Installation
------------
1. Enable this module and go to the http://your-drupal-site-path/admin/structure/services.

That's it! Just configure the resource as you would normally.


Credits and Maintenance
-----------------------
Paulo Carvalho, Mobile & Web Developer.
Info: http://www.paulocarvalho.pt
Twitter: @ut_neon